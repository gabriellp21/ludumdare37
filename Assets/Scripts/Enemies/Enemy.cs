﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy : MonoBehaviour, Damageable, Killable
{

    enum State
    {
        IDLE,
        ATTACKING,
        FOLLOWING,
        DYING
    }
    
    [SerializeField]
    private HpBar hpBar;

    [SerializeField]
    private float maxHp;

    private EnemyFollow enemyFollow;

    private float amountHp;

    private Renderer renderer;

    private float attackCycle = 0;

    private State state = State.IDLE;


	// Use this for initialization
	void Start ()
    {
        amountHp = maxHp;
        renderer = GetComponent<Renderer>();
        enemyFollow = GetComponent<EnemyFollow>();

    }
	
	// Update is called once per frame
	void Update ()
    {

        renderer.sortingOrder = (int)Camera.main.WorldToScreenPoint(renderer.bounds.min).y * -1;
        
        switch (state)
        {
            case State.ATTACKING:

                updateAttack();

                break;
            case State.FOLLOWING:

                break;
            case State.IDLE:

                break;
            case State.DYING:

                break;
        }
	}

    /// <summary>
    /// Update Hp and UI (HpBar)
    /// </summary>
    /// <param name="amount">Amoutn of damage</param>
    public void hit(float amount)
    {
        amountHp -= amount;

        float fillAmount = 0;

        if (amountHp > 0)
        {
            fillAmount = amountHp / maxHp;
        }

        if (amountHp <= 0) {
            die();
        }

        if (hpBar != null)
        {
            hpBar.updateHp(fillAmount);
        }
    }

    /// <summary>
    /// Start attack cycle
    /// </summary>
    public void attack()
    {

        state = State.ATTACKING;

        attackCycle = 200;
    }

    /// <summary>
    /// Start follow cycle
    /// </summary>
    public void follow()
    {
        state = State.FOLLOWING;

        updateAnimation();
    }


    /// <summary>
    /// Start idle cycle
    /// </summary>
    public void idle()
    {
        state = State.IDLE;

        updateAnimation();
    }

    private void updateAttack()
    {
        attackCycle++;

        if (attackCycle >= 200)
        {

            GameObject target = null;

            if (!target && enemyFollow != null)
            {
                target = enemyFollow.getTarget();
            }

            if (!target)
            {
                return;
            }

            updateAnimation("attacking");

            attackCycle = 0;
        }
    }


    /// <summary>
    /// Apply damage to target
    /// </summary>
    public void doAttack()
    {
		if(enemyFollow.getTarget() == null) {
			return;
		}

        var damageable = enemyFollow.getTarget().GetComponent<Damageable>();

        if (damageable != null)
        {

            damageable.hit(1);
        }
    }

    /// <summary>
    /// Update animation state
    /// </summary>
    private void updateAnimation()
    {
        var animator = GetComponent<Animator>();

        if (animator)
        {
            switch (state)
            {
                case State.FOLLOWING:
                    animator.SetTrigger("walk");
                    break;
                case State.ATTACKING:
                case State.IDLE:
                    animator.SetTrigger("idle");
                    break;

            }
        }
    }

    /// <summary>
    /// Update animation state based on trigger.
    /// </summary>
    private void updateAnimation(string animState)
    {
        
        if (animState == null)
        {
            updateAnimation();
        }
        else
        {
            var animator = GetComponent<Animator>();

            if (animator)
            {
                animator.Play(animState);
            }
        }
    }

    public void die()
    {
        if (amountHp <= 0)
        {
            removeColliders();
            state = State.DYING;
            updateAnimation("dying");
            enemyFollow.stopFollow();
            enemyFollow.setTarget(null);
        }
    }   

    public void die(string reason)
    {
        if (reason.Equals(""))
        {
            die();

        } else
        {
            switch (reason)
            {
                case "pee":

                    removeColliders();

                    state = State.DYING;
                    updateAnimation("dyingByPee");
                    enemyFollow.stopFollow();
                    enemyFollow.setTarget(null);
                    break;
                default:
                    die();
                    break;
            }
        }
    }

    public void removeCorpse()
    {
        Destroy(gameObject);

        int dropItemNumber = UnityEngine.Random.Range(1, 60);

        if (dropItemNumber == 15)
        {
            Instantiate(Resources.Load("Ammo/ShotgunBullets"), transform.position, Quaternion.identity);
        }

        if (dropItemNumber == 16)
        {
            Instantiate(Resources.Load("Ammo/PoopBombFood"), transform.position, Quaternion.identity);
        }

        if (dropItemNumber == 17)
        {
            Instantiate(Resources.Load("Ammo/AcidPeeWater"), transform.position, Quaternion.identity);
        }

		if (dropItemNumber == 18)
		{
			Instantiate(Resources.Load("Recovers/DogCookieRecover"), transform.position, Quaternion.identity);
		}

		if (dropItemNumber == 19)
		{
			Instantiate(Resources.Load("Recovers/DogOwnerRecover"), transform.position, Quaternion.identity);
		}

        GameObject.Find ("Player").GetComponent<PlayerMovement>().score += 100;
    }


    /// <summary>
    /// Remove colliders, useful for die event
    /// </summary>
    private void removeColliders()
    {
        var circleCollider = GetComponent<CircleCollider2D>();
        var boxCollider = GetComponent<BoxCollider2D>();

        if (circleCollider != null)
        {
            circleCollider.enabled = false;
        }

        if (boxCollider != null)
        {
            boxCollider.enabled = false;
        }
    }
}
