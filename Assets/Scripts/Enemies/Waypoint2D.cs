﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Waypoint2D
{

    [SerializeField]
    private Vector2 position;

    private bool marked = false;

    /// <summary>
    /// Create empty instance.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public Waypoint2D create(Vector2 position)
    {
        return new Waypoint2D(position);
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="position"></param>
    public Waypoint2D (Vector2 position)
    {
        this.position = position;    
    }

    /// <summary>
    /// Get the distance of the waypoint from the gameobject.
    /// </summary>
    /// <param name="gameObject"></param>
    /// <returns></returns>
    public float getDistanceFromGameObject(GameObject gameObject)
    {
        Vector2 comparePosition = (Vector2)gameObject.transform.position;
        return Vector2.Distance(position, comparePosition);
    }

    /// <summary>
    /// Get the waypoint position 2d.
    /// </summary>
    /// <returns></returns>
    public Vector2 getPosition()
    {
        return this.position;
    }

    /// <summary>
    /// Marks the waypoint
    /// </summary>
    /// <param name="marked"></param>
    public void setMarked(bool marked = false)
    {
        this.marked = marked;
    }


    /// <summary>
    /// Return if waypoint is marked
    /// </summary>
    /// <returns></returns>
    public bool isMarked()
    {
        return this.marked;
    }
}
