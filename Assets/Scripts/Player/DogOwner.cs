﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DogOwner : MonoBehaviour, Damageable, Killable {


    [SerializeField]
    private HpBar hpBar;

    [SerializeField]
    private float maxHp;

    private float amountHp;

    private Renderer renderer;


    // Use this for initialization
    void Start ()
    {
        amountHp = maxHp;
        renderer = GetComponent<Renderer>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        renderer.sortingOrder = (int)Camera.main.WorldToScreenPoint(renderer.bounds.min).y * -1;
    }

    public void die()
    {

    }

    public void die(string reason)
    {
        die();
    }

    public void hit(float amount)
    {
        amountHp -= amount;

        float fillAmount = 0;

        if (amountHp > 0)
        {
            fillAmount = amountHp / maxHp;

            int randGrunt = Random.Range(1, 3);

            GetComponent<AudioSource>().PlayOneShot(Resources.Load<AudioClip>("Audio/poo_grunt_" + randGrunt));
        }
        else
        {
            SceneManager.LoadScene("GameOver");
        }

        hpBar.updateHp(fillAmount);
    }

	public void recoverDogOwnerHP(float amount)
	{
		amountHp += amount;

		float fillAmount = amountHp / maxHp;

		GetComponent<AudioSource>().PlayOneShot(Resources.Load<AudioClip>("Audio/poo_grunt_"));

		hpBar.updateHp(fillAmount);
	}
}
