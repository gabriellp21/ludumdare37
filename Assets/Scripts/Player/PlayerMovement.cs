﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	//Variáveis para limitar posição do player
	private Vector3 playerSize;
	private float distance;
	private float leftBorder, rightBorder, bottomBorder, upBorder;
	Vector3 positionPlayer = new Vector3(0, 0, 0);

	public float speed = 5f;
	private Vector3 previousPosition;
	private float velocity = 0;
	private bool facingRight = false;
	public CameraController cameraController;

    public GameObject poopPrefab;
	private int poopCharges;
	public Text poopAmmoText;
    private bool canThrowPoop = true;

    public GameObject peePrefab;
    private int peeCharges;
	public Text peeAmmoText;
    private bool canPee = true;
    private bool canMove = true;

	[HideInInspector]
	public float score;
	public Text scoreAmountText;

	void Start()
	{
		peeCharges = 5;
		poopCharges = 5;
		score = 0;

		//Definindo os limites do player atraves da visão da camera
		if (Camera.main != null) {
			playerSize = transform.GetComponent<SpriteRenderer> ().bounds.size;
			distance = (transform.position - Camera.main.transform.position).z;
			leftBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, distance)).x + (playerSize.x / 2);
			rightBorder = Camera.main.ViewportToWorldPoint (new Vector3 (1, 0, distance)).x - (playerSize.x / 2);
			bottomBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, distance)).y + (playerSize.y / 2);
			upBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 1, distance)).y - (playerSize.y / 2);
			positionPlayer = transform.position;
		}
	}

	void Update()
	{
		InputMovement ();
		peeAmmoText.text = "X"+peeCharges.ToString();
		poopAmmoText.text = "X"+poopCharges.ToString();
		scoreAmountText.text = score.ToString ();
	}

	void InputMovement()
	{
        if (canMove)
        {
			if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                transform.Translate(Vector3.up * speed * Time.deltaTime);
            }

			if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                transform.Translate(Vector3.down * speed * Time.deltaTime);
            }

			if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                if (facingRight)
                {
                    transform.Translate(Vector2.right * speed * Time.deltaTime);
                }
                else
                {
                    transform.Translate(Vector2.left * speed * Time.deltaTime);
                }
            }

			if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                if (facingRight)
                {
                    transform.Translate(Vector2.left * speed * Time.deltaTime);
                }
                else
                {
                    transform.Translate(Vector2.right * speed * Time.deltaTime);
                }
            }

			if (Input.GetKey(KeyCode.Escape)) {
				SceneManager.LoadScene ("Splash");
			}

			if (Input.GetMouseButton(1))
            {
                if (poopCharges > 0 && canThrowPoop)
                {
                    canThrowPoop = false;
                    StartCoroutine(throwPoop());
					poopCharges--;
                }
            }

			if (Input.GetKey(KeyCode.Space))
            {
                if (peeCharges > 0 && canPee)
                {
                    startPee();
					peeCharges--;
                }
            }

            if (Input.GetKey(KeyCode.Alpha1))
            {
                transform.Find("Weapon/Shotgun").gameObject.SetActive(false);
                transform.Find("Weapon/Pistol").gameObject.SetActive(true);
            }

            if (Input.GetKey(KeyCode.Alpha2))
            {
                transform.Find("Weapon/Pistol").gameObject.SetActive(false);
                transform.Find("Weapon/Shotgun").gameObject.SetActive(true);
            }
        }

		velocity = ((transform.position - previousPosition).magnitude) / Time.deltaTime;
		previousPosition = transform.position;

		if (velocity > 1) {
			GetComponent<Animator> ().SetTrigger ("walk");
		} else {
			GetComponent<Animator> ().SetTrigger ("idle");
		}

		Vector3 mousePosition = Input.mousePosition;
		mousePosition.z = 10f;
		mousePosition = Camera.main.ScreenToWorldPoint (mousePosition);
		Vector3 calculatedMousePosition = mousePosition - transform.position;

		if (calculatedMousePosition.x > 0) {
			facingRight = true;
			transform.eulerAngles = Vector3.zero;
		} else {
			facingRight = false;
			transform.eulerAngles = new Vector3 (0, -180, 0);
		}

		transform.Find ("UI").transform.rotation = Quaternion.identity;

		transform.position = new Vector3 (
			Mathf.Clamp (transform.position.x, leftBorder, rightBorder), 
			Mathf.Clamp (transform.position.y, bottomBorder, upBorder),
			transform.position.z
		);
	}

    IEnumerator throwPoop()
    {
        var targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        GameObject poop = (GameObject)Instantiate(poopPrefab, transform.position, transform.rotation);

        var poopBomb = poop.GetComponent<PoopBomb>();

        if (poopBomb != null)
        {
            poopBomb.throwBomb(transform.position, targetPosition);
        }

        yield return new WaitForSeconds(1f);
        canThrowPoop = true;
    }

    /// <summary>
    /// Start pees animation
    /// </summary>
    public void startPee()
    {
        canPee = false;
        canMove = false;
        GetComponent<Animator>().SetTrigger("pee");
    }

    /// <summary>
    /// Make the pee prefab
    /// </summary>
    public void makePee()
    {
        Vector2 peePosition = new Vector3(transform.position.x + 0.5f,
            transform.position.y - 0.5f, 0);

        if (!facingRight)
        {
            peePosition = new Vector3(transform.position.x - 0.5f,
                        transform.position.y - 0.5f, 0);
        }

        GameObject pee = (GameObject)Instantiate(peePrefab, peePosition, transform.rotation);
    }

    /// <summary>
    /// Recover move status and cowndown
    /// </summary>
    public void finishPee()
    {
        canPee = true;
        canMove = true;
    }

	public void addPeeCharge() {
		peeCharges += 5;
	}

	public void addPoopCharge() {
		poopCharges += 3;
	}
}