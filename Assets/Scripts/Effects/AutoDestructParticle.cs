﻿using UnityEngine;
using System.Collections;

public class AutoDestructParticle : MonoBehaviour {

    private ParticleSystem particle;

	// Use this for initialization
	void Start ()
    {
        particle = GetComponent<ParticleSystem>();
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        
        if (particle != null)
        {
            if (!particle.IsAlive())
            {
                Destroy(gameObject);
            }
        }
	}
}
