﻿using UnityEngine;
using System.Collections;

public class RoomController : MonoBehaviour {

	public int numberWaveEnemies = 100;
	public int spawnTimeRate = 6;
    public int maxSpawnEnemies = 16;


	public GameObject followTarget;
    public GameObject dogOwnerTarget;

	// Use this for initialization
	void Start () {
        InvokeRepeating ("callSpawnEnemies", 3, spawnTimeRate);
        StartCoroutine(spawnEnemies());
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

	private void callSpawnEnemies () {
		StartCoroutine ("spawnEnemies");
	}

	private IEnumerator spawnEnemies() {
		for (int i = 0; i < maxSpawnEnemies; i++) {
			GameObject spawnPoint = GameObject.Find ("SpawnPoint"+i);

			for (int k = 0; k < numberWaveEnemies; k++) {

                float num = Random.Range(1, 4);
                float numFollow = Random.Range(1, 4);

                string resource = "";

                if (num == 1)
                {
                    resource = "Enemies/Enemy01";
                } else if (num == 2)
                {
                    resource = "Enemies/Enemy02";
                } else
                {
                    resource = "Enemies/Enemy03";
                }

				GameObject enemy = (GameObject)Instantiate(
					Resources.Load(resource), 
					new Vector3(spawnPoint.transform.position.x, spawnPoint.transform.position.y, 0),
					Quaternion.identity
				);

                if (numFollow % 2 == 0)
                {
                    enemy.GetComponent<EnemyFollow>().setTarget(dogOwnerTarget, true);
                } else
                {
				    enemy.GetComponent<EnemyFollow> ().setTarget (followTarget, true);
                }
			}

			yield return false;
		}
	}
}
