﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SplashController : MonoBehaviour {

	void Start() {
		Cursor.SetCursor(Resources.Load<Texture2D>("Sprites/UI/Cursor"), Vector2.zero, CursorMode.Auto);
	}

	// Update is called once per frame
	void Update () {
		if(Input.anyKey) {
			SceneManager.LoadScene("Instructions");
		}
	}
}
