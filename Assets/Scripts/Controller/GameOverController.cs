﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameOverController : MonoBehaviour {

	private bool canRestart = false;
	public GameObject pressAnyButtonText;

	// Use this for initialization
	void Start () {
		StartCoroutine ("enableRestart");
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.anyKey && canRestart) {
			SceneManager.LoadScene("Splash");
		}
	}

	private IEnumerator enableRestart() {
		yield return new WaitForSeconds (2f);
		canRestart = true;
		pressAnyButtonText.SetActive (true);
	}
}
