﻿using UnityEngine;
using System.Collections;

public class AcidPeeAmmo : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy (gameObject, 10f);
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player") {			
			coll.gameObject.GetComponent<PlayerMovement> ().addPeeCharge ();
			Destroy (gameObject);
		}
	}
}
