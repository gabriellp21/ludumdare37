﻿using UnityEngine;
using System.Collections;

public class ShotgunAmmo : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy (gameObject, 10f);
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player") {			
			coll.transform.Find ("Weapon/Shotgun").GetComponent<Shotgun> ().addBullets ();
			Destroy (gameObject);
		}
	}
}
