﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HpBar : MonoBehaviour {

    public Image hpFill;

	// Use this for initialization
	void Start ()
    {
        
	}
	
    /// <summary>
    /// Update amount of fill image
    /// </summary>
    /// <param name="amount"></param>
    public void updateHp(float amount)
    {
        hpFill.fillAmount = amount;
    }

}
