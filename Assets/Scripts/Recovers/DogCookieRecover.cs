﻿using UnityEngine;
using System.Collections;

public class DogCookieRecover : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy (gameObject, 10f);
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player") {			
			coll.gameObject.GetComponent<PlayerHealth> ().recoverDogHP (3);
			Destroy (gameObject);
		}
	}
}
