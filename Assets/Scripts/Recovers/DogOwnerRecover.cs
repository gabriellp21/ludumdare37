﻿using UnityEngine;
using System.Collections;

public class DogOwnerRecover : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy (gameObject, 10f);
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player") {
			GameObject.Find ("DogOwner").GetComponent<DogOwner> ().recoverDogOwnerHP (3);
			Destroy (gameObject);
		}
	}
}
