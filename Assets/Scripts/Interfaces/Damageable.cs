﻿using UnityEngine;
using System.Collections;

public interface Damageable
{

    void hit(float amountDamage);
}
