﻿using UnityEngine;
using System.Collections;

public interface Killable
{

    void die();
    void die(string reason);
}
