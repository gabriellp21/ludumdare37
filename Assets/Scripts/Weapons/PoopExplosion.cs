﻿using UnityEngine;
using System.Collections;

public class PoopExplosion : MonoBehaviour {

    void Start()
    {
        var cameraController = FindObjectOfType<CameraController>();
        GetComponent<AudioSource>().PlayOneShot(Resources.Load<AudioClip>("Audio/poop_explosion"));

        if (cameraController != null)
        {
            cameraController.shakeCamera(0.5f, 1);
        }
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            return;
        }

        var damageable = other.GetComponent<Damageable>();

        if (damageable != null)
        {
            damageable.hit(4);
        }
    }
}
