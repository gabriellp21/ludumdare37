﻿using UnityEngine;
using System.Collections;

public class ShotgunSpread : MonoBehaviour {

    void OnParticleCollision(GameObject other)
    {
		if (other.tag == "Player") {
			return;
		}

        var damageable = other.GetComponent<Damageable>();

        if (damageable != null)
        {
            damageable.hit(3);
        }
    }
}
