﻿using UnityEngine;
using System.Collections;

public class PoopBomb : MonoBehaviour {

    private bool thrown = false;

    private float time = 0;

    private Vector3 initialPosition;

    private Vector3 finalPosition;

    [SerializeField]
    private GameObject explosionPrefab;

    void OnEnable()
    {
        thrown = false;
        time = 0;
    }
	
	// Update is called once per frame
	void Update () {

	    if (thrown)
        {
            time +=  Time.deltaTime;
            transform.position = SampleParabola(initialPosition, finalPosition, 1, time);

            if (Vector3.Distance(transform.position, finalPosition) <= 0.2)
            {
                if (explosionPrefab != null)
                {
                    Instantiate(explosionPrefab, transform.position, transform.rotation);
                }

                Destroy(gameObject);
            }
        }
	}

    public void throwBomb(Vector3 from, Vector3 to)
    {
        initialPosition = from;
        finalPosition = to;

        thrown = true;
    }


    /// <summary>
    /// Calculates parabolas movement
    /// https://forum.unity3d.com/threads/generating-dynamic-parabola.211681/
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="height"></param>
    /// <param name="t"></param>
    /// <returns></returns>
    private Vector3 SampleParabola(Vector3 start, Vector3 end, float height, float t)
    {
        if (Mathf.Abs(start.y - end.y) < 0.1f)
        {
            //start and end are roughly level, pretend they are - simpler solution with less steps
            Vector3 travelDirection = end - start;
            Vector3 result = start + t * travelDirection;
            result.y += Mathf.Sin(t * Mathf.PI) * height;
            return result;
        }
        else
        {
            //start and end are not level, gets more complicated
            Vector3 travelDirection = end - start;
            Vector3 levelDirecteion = end - new Vector3(start.x, end.y, start.z);
            Vector3 right = Vector3.Cross(travelDirection, levelDirecteion);
            Vector3 up = Vector3.Cross(right, travelDirection);
            if (end.y > start.y) up = -up;
            Vector3 result = start + t * travelDirection;
            result += (Mathf.Sin(t * Mathf.PI) * height) * up.normalized;
            return result;
        }
    }
}
