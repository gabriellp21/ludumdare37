﻿using UnityEngine;
using System.Collections;

public class Pistol : WeaponBase {

    /// <summary>
    /// Fire's the weapong
    /// </summary>
    protected override void shot()
    {
        if (shotEffect != null && spawnEffectPosition != null)
        {
            float angle = getTargetDirectionAngle();
            angle = angle + 180 - (2 * angle) + 180;

            Instantiate(shotEffect, spawnEffectPosition.position, Quaternion.Euler(new Vector3(angle, 90, 0)), null);

            GetComponent<AudioSource>().PlayOneShot(Resources.Load<AudioClip>("Audio/handgun_fire"));
        }
    }
}
