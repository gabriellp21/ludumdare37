﻿using UnityEngine;
using System.Collections;

public class WeaponBase : MonoBehaviour {

    [SerializeField]
    private Sprite zeroAngleSprite;

    [SerializeField]
    private Sprite neAngleSprite;

    [SerializeField]
    private Sprite seAngleSprite;

    [SerializeField]
    private bool mustRotate = false;

    public GameObject shotEffect;

    public Transform spawnEffectPosition;


    private SpriteRenderer spriteRenderer;

    private Renderer renderer;
    private Renderer rootRenderer;

    // Use this for initialization
    protected virtual void Start ()
    {
        rootRenderer = transform.root.GetComponent<Renderer>();
        renderer = GetComponent<Renderer>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
	protected virtual void Update ()
    {
        renderer.sortingOrder = (int)Camera.main.WorldToScreenPoint(rootRenderer.bounds.min).y * -1 + 10;

        if (mustRotate)
        {
            rotateMouseDirection();
        }

        // Left click
        if (Input.GetMouseButtonDown(0))
        {
            shot();
        }
    }

    /// <summary>
    /// Rotate the weapon base on mouse position
    /// </summary>
    protected void rotateMouseDirection()
    {
        float angle = getTargetDirectionAngle();
        float tmpAngle = angle;

        if (angle < 0)
        {
            angle += 360;
        }

        if (angle >= 270 && angle <= 360)
        {
            renderer.sortingOrder = ((int)Camera.main.WorldToScreenPoint(rootRenderer.bounds.min).y * -1) + 1;

        } else if (angle >= 0 && angle <= 180)
        {
            renderer.sortingOrder = ((int)Camera.main.WorldToScreenPoint(rootRenderer.bounds.min).y * -1) - 1;

        } else
        {
            renderer.sortingOrder = ((int)Camera.main.WorldToScreenPoint(rootRenderer.bounds.min).y * -1);
        }

        if (!(angle >= 0 && angle <= 90) 
            && !(angle >= 270 && angle <= 360))
        {
            tmpAngle = angle + 180 - (2 * angle);

        }
			
        updateSpriteByAngle(angle);

        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x,
                                                       transform.localEulerAngles.y,
                                                       tmpAngle);
    }

    /// <summary>
    /// Change sprite based on angle direction
    /// </summary>
    /// <param name="angle">float</param>
    protected void updateSpriteByAngle(float angle)
    {
        if ((angle >= 330 && angle <= 360) 
            || (angle >= 0 && angle <= 30)
            || (angle >= 150 && angle <= 210))
        {
            if (zeroAngleSprite != null)
            {
                // Make Change
                spriteRenderer.sprite = zeroAngleSprite;
            }

        } else if ((angle >= 210 && angle <= 330))
        {
            if (seAngleSprite != null)
            {
                // Make Change
                spriteRenderer.sprite = seAngleSprite;
            }
        } else
        {
            if (neAngleSprite != null)
            {
                // Make Change
                spriteRenderer.sprite = neAngleSprite;
            }
        }
    }

    /// <summary>
    /// Get angle based on mouse direction
    /// </summary>
    /// <returns>float</returns>
    protected float getTargetDirectionAngle()
    {
        float angle = 0;

        Vector3 cursorPosition = Input.mousePosition;
        cursorPosition.z = 10;

        var mousePosition = Camera.main.ScreenToWorldPoint(cursorPosition);
        var relative = mousePosition - transform.position;

        angle = Mathf.Atan2(relative.y, relative.x) * Mathf.Rad2Deg;

        if (angle < 0)
        {
            angle += 360;
        }

        return angle;
    }

    /// <summary>
    /// Get angle based on mouse direction
    /// </summary>
    /// <returns>float</returns>
    protected float getRealDirectionAngle()
    {
        float angle = 0;

        Vector3 cursorPosition = Input.mousePosition;
        cursorPosition.z = 10;

        var mousePosition = Camera.main.ScreenToWorldPoint(cursorPosition);
        var relative = mousePosition - transform.position;

        angle = Mathf.Atan2(relative.y, relative.x) * Mathf.Rad2Deg;

        if (angle < 0)
        {
            angle += 360;
        }

        float tmpAngle = angle;

        if (!(angle >= 0 && angle <= 90)
            && !(angle >= 270 && angle <= 360))
        {
            tmpAngle = angle + 180 - (2 * angle);

        }

        return tmpAngle;
    }

    protected virtual void shot() {
        // Must be implemented by inheritance
    }
}