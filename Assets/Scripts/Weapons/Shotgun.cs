﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Shotgun : WeaponBase {

    private bool canShot = true;
	public Text bulletAmmoText;

	[HideInInspector]
	public int bullets;

    void OnEnable()
    {
        canShot = true;
    }

	protected override void Start() {
		base.Start ();
		bullets = 10;
		InvokeRepeating ("displayBullets", 0f, 0.1f);
	}

    /// <summary>
    /// Fire's the weapong
    /// </summary>
    protected override void shot()
    {

        if (!canShot)
        {
            return;
        }

		if (bullets == 0) {
			GetComponent<AudioSource>().PlayOneShot(Resources.Load<AudioClip>("Audio/no_bullets"));
			return;
		}

        if (shotEffect != null && spawnEffectPosition != null)
        {
            canShot = false;

            float angle = getTargetDirectionAngle();
            angle = angle + 180 - (2 * angle) + 180;

            Instantiate(shotEffect, spawnEffectPosition.position, Quaternion.Euler(new Vector3(angle, 90, 0)), null);

            GetComponent<AudioSource>().PlayOneShot(Resources.Load<AudioClip>("Audio/shotgun_fire"));
            StartCoroutine("playShotgunPump");
			bullets--;
        }
    }

    private IEnumerator playShotgunPump()
    {
        yield return new WaitForSeconds(0.3f);
        GetComponent<AudioSource>().PlayOneShot(Resources.Load<AudioClip>("Audio/shotgun_pump"));
        yield return new WaitForSeconds(0.2f);
        canShot = true;
    }

	public void addBullets() {
		bullets += 10;
		GetComponent<AudioSource>().PlayOneShot(Resources.Load<AudioClip>("Audio/insert_bullet"));
	}

	private void displayBullets() {
		bulletAmmoText.text = "X"+bullets.ToString();
	}
}
