﻿using UnityEngine;
using System.Collections;

public class PistolBullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnParticleCollision(GameObject other)
    {
		if (other.tag == "Player") {
			return;
		}

        var damageable = other.GetComponent<Damageable>();

        if (damageable != null)
        {
            damageable.hit(7);
        }
    }
}
